$(document).on('click','#planesytours', function(e) {
	e.preventDefault();
	$('.section-app.menu').css('right', 0);
});

$(document).on('click','#volver_1', function(e) {
	e.preventDefault();
	$('.section-app.menu').css('right', -1500);
});

$(document).on('click','#volver_2', function(e) {
	e.preventDefault();
	$('.section-app.single').css('right', -1500);
});

$(document).on('click','#volver_3', function(e) {
	e.preventDefault();
	$('.section-app.formulario').css('right', -1500);
});

$(document).on('click','#Enviar', function(e) {
	e.preventDefault();
	alert('Gracias por reservar tu plan con nosotros, pronto nos contactaremos con tigo');
	$('.section-app.formulario').css('right', -1500);
});




$(document).on('click','#Plan', function(e) {
	e.preventDefault();
	$('.text').each(function () {
		$(this).css('display', 'none');		
	});
	$('.text.'+$(this).attr('plan')).css('display', 'block');
	$('#TitleDestino').text($(this).attr('titulo'));
	$('.section-app.single').attr('plan', $(this).attr('plan'));
	$('#ImagenBanner').attr('src', $(this).attr('imgDestino'));

	$('.section-app.single').css('right', 0);
});


$('#reservar').click(function (e) {
	e.preventDefault();
	$('.section-app.formulario').css('right', 0);
});


// poner corazon rojo en single
$(document).on('click','#me-gusta', function(e) {
	e.preventDefault();
	if ($(this).children('span').hasClass('active')) {
		$(this).children('span').removeClass('active');
	}else {
		$(this).children('span').addClass('active');
	}
});

$(document).on('click','#conocer-mas', function(e) {
	e.preventDefault();
	var p = $('.section-app.single').attr('plan');
	if ($('.text.'+$('.section-app.single').attr('plan')+'>.t2').is(':visible')) {
		$("#conocer-mas").text("Conocer más");
	}else {
		$("#conocer-mas").text("Ver menos");
	}
	$('.text.'+$('.section-app.single').attr('plan')+'>.t2').slideToggle('slow');
});
