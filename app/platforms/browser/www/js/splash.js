
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

   
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        // constructor splash
        function show_popup(){ 
        window.location="index.html";
        }; 
        window.setTimeout(show_popup, 6000); // 6 seconds 
        
    }
};

app.initialize();